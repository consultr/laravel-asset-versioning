<?php

if (! function_exists('asset_path')) {
    /**
     * Get the path to a versioned asset file.
     *
     * @param  string  $file
     * @return string
     */
    function asset_path($file)
    {
        $file = trim($file, '/');
        $config = config(config('services.asset-version.config', 'asset-version'));
        $version = 1;
        if(is_array($config)) {
            if(array_key_exists('version',$config)) {
                $version = $config['version'];
            }
        }
        $file = $file.'?v='.$version;

        return asset($file);
    }
}
